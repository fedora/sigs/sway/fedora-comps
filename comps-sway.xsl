<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common" version="1.0" extension-element-prefixes="exsl">
  <xsl:strip-space elements="*"/>
  <xsl:output method="xml" indent="yes" encoding="UTF-8" doctype-system="comps.dtd" doctype-public="-//Red Hat, Inc.//DTD Comps info//EN"/>
  <!-- Preserve most nodes -->
  <xsl:template match="*" priority="0">
    <xsl:apply-templates select="." mode="normalize"/>
  </xsl:template>
  <xsl:template match="*" mode="normalize">
    <!-- Group comments with the logically-following element -->
    <xsl:apply-templates select="preceding-sibling::node()[normalize-space()][1][self::comment()] "/>
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()"/>
    </xsl:copy>
  </xsl:template>
  <!-- Preserve attributes and text nodes -->
  <xsl:template match="comment()|text()">
    <xsl:apply-templates select="preceding-sibling::node()[normalize-space()][1][self::comment()] "/>
    <xsl:copy/>
  </xsl:template>
  <!-- Preserve attributes -->
  <xsl:template match="@*">
    <xsl:copy/>
  </xsl:template>

  <xsl:template match="/comps/group[not(starts-with(id,'sway'))]" />
  <xsl:template match="/comps/environment[not(starts-with(id,'sway'))]" />
  <xsl:template match="/comps/category[not(starts-with(id,'sway'))]" />
  <xsl:template match="/comps/langpacks" />
</xsl:stylesheet>
